angular.module('cardapioClientApp.services', [])

.factory('ApiRestangular', function(Restangular, API) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(API.db);
    });
})

.factory('CLIENTS',function(ApiRestangular){
    return ApiRestangular.service('clients');
})

.filter('uploadDir', function(API) {
    return function(url) {
        return API.upload + url;
    };
});
